package com.sinyolo.smartmerchandiser;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinyolo.smartmerchandiser.model.MerchandiserActivity;
import com.sinyolo.smartmerchandiser.model.Stock;
import com.sinyolo.smartmerchandiser.model.StockActivity;
import com.sinyolo.smartmerchandiser.utils.ImsiProperty;
import com.sinyolo.smartmerchandiser.utils.MethodNotFoundException;
import com.sinyolo.smartmerchandiser.utils.TelephonyInfo;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by leeroy on 9/3/2018.
 */

public class SinyoloUtils {
    //
    public static final String REMOTE_URL = "http://192.168.110.1:4556/sizo/centralized-merchandising-system/v1/api/processrequest";
    public static String SUPERMARKET = "SABLE SUPERMARKET";
    public static String MERCHANDISER_NAME = "SABLE MERCHANDISER";
    public static String ORDER_STRING = "";
    public static final String IMSI_1 = "IMSI_1";
    public static final String IMSI_2 = "IMSI_2";
    public static String IMSI_IN_USE = "DEFAULT_IMSI";
    public static String ERROR = "DEFAULT_ERROR";
    public static final String HEADER_DATA = "HEADER_DATA";
    public static final String CHILD_DATA = "CHILD_DATA";

    public static <T> T fromJson(String jsonString, Class<T> type) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(jsonString, type);
        } catch (IOException e) {
            return null;
        }
    }

    public static List<String> defaultStock(List<StockActivity> stockHistory) {
        List<String> stringList = new ArrayList<>();

        Stock stock;
        for (int i = 0; i <= 9; i++) {
            stock = new Stock();
            stock.setName("Default Name " + i);
            stock.setQuantity(Long.valueOf(i));
            stock.setValue(BigDecimal.valueOf(i));

            stock.setAlarmQuantity(2l);
            stock.setOptimumQuanity(5l);
            stock.setStockHistory(stockHistory);

            stringList.add(stock.toString());
        }
        return stringList;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static HashMap<String, List<String>> defaultStockActivity(List<String> stockList) {
        HashMap<String, List<String>> stockActivityMap = new HashMap<>();

        for (String stock : stockList) {
            List<String> stockActivityList = new ArrayList<>();
            StockActivity stockActivity;
            for (int i = 0; i < 9; i++) {
                stockActivity = new StockActivity();
                stockActivity.setActivity("Default Activity " + i);
                stockActivity.setActivityBy(String.valueOf(i));
                stockActivity.setUpdated(new Date());

                stockActivityList.add(stockActivity.toString());
            }
            stockActivityMap.put(stock.toString(), stockActivityList);
        }

        return stockActivityMap;
    }

    public static ImsiProperty getImsiProperty(Context context) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, MethodNotFoundException, InvocationTargetException {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast toast = Toast.makeText(context, "Missing READ PHONE STATE permission!", Toast.LENGTH_LONG);
            toast.show();
            return null;
        }

        ImsiProperty imsi = new ImsiProperty();

        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(context);

        boolean isDualSIM = telephonyInfo.isDualSIM();

        if (!isDualSIM) {

            TelephonyManager tel = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);

            if (tel.getSubscriberId() != null) {
                imsi.setImsi(tel.getSubscriberId());
            } else {
                String android_id = Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                imsi.setImsi(android_id);
            }
        } else {

            String imeiSIM1 = telephonyInfo.getImsiSIM1();
            String imeiSIM2 = telephonyInfo.getImsiSIM2();

            boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
            boolean isSIM2Ready = telephonyInfo.isSIM2Ready();

            imsi.getImsis().put(IMSI_1, imeiSIM1);
            imsi.getImsis().put(IMSI_2, imeiSIM2);
        }
        return imsi;
    }

    public static void checkPermissions(Context context, String permission) {
        if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast toast = new Toast(context);
            toast.setText("Missing READ PHONE STATE permission!");
            toast.show();
        }
    }


    @TargetApi(Build.VERSION_CODES.O)
    public static HashMap<String, List<String>> defaultMerchandiserActivity(List<String> merchandisers) {
        HashMap<String, List<String>> merchandisersMap = new HashMap<>();

        for (String merchandiser : merchandisers) {
            List<String> merchandiserActivities = new ArrayList<>();
            MerchandiserActivity merchandiserActivity;
            for (int i = 0; i < 9; i++) {
                merchandiserActivity = new MerchandiserActivity();
                merchandiserActivity.setActivity("Default Activity " + i);
                merchandiserActivity.setUpdated(new Date());

                merchandiserActivities.add(merchandiserActivity.toString());
            }

            merchandisersMap.put(merchandiser, merchandiserActivities);
        }
        return merchandisersMap;
    }

    public static boolean isListEmpty(List list) {
        return list == null || list.isEmpty();
    }
}
