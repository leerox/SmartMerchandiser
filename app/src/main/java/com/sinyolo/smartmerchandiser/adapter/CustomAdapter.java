package com.sinyolo.smartmerchandiser.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sinyolo.smartmerchandiser.R;
import com.sinyolo.smartmerchandiser.model.Stock;
import com.sinyolo.smartmerchandiser.model.StockActivity;

import java.util.List;

/**
 * Created by leeroy on 9/3/2018.
 */
public class CustomAdapter<T> extends ArrayAdapter<T> {
    private Context mContext;
    private int id;
    private List<T> items;

    public CustomAdapter(Context context, int textViewResourceId, List<T> list) {
        super(context, textViewResourceId, list);
        mContext = context;
        id = textViewResourceId;
        items = list;

    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        View mView = v;
        View mNestedView;
        if (mView == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = vi.inflate(id, null);
            mNestedView = vi.inflate(android.R.layout.simple_list_item_1, null);
        }

        TextView mText = (TextView) mView.findViewById(R.id.customTextView);
        ListView mList = (ListView) mView.findViewById(R.id.customList);

        if (items.get(position) != null) {
            if (items.get(position) instanceof Stock) {
                if (((Stock) items.get(position)).getQuantity() <= ((Stock) items.get(position)).getAlarmQuantity()) {
                    mText.setBackgroundColor(Color.RED);
                } else if (((Stock) items.get(position)).getQuantity() < ((Stock) items.get(position)).getOptimumQuanity()) {
                    mText.setBackgroundColor(Color.rgb(254, 178, 4));//AMBER
                } else {
                    mText.setBackgroundColor(Color.GREEN);
                }

                if (((Stock) items.get(position)).getStockHistory() != null && !((Stock) items.get(position)).getStockHistory().isEmpty()) {

                    ArrayAdapter<StockActivity> historyAdapter = new ArrayAdapter<StockActivity>(mContext, android.R.layout.simple_list_item_1,
                            ((Stock) items.get(position)).getStockHistory().toArray(new StockActivity[]{}));
                    mList.setAdapter(historyAdapter);
                }
            } else {
                mText.setBackgroundColor(Color.rgb(132, 112, 255));//LIGHT STATE BLUE
                mList.setAdapter(null);
            }
            mText.setTextColor(Color.WHITE);
            mText.setText(items.get(position).toString());
        }

        return mView;
    }
}
