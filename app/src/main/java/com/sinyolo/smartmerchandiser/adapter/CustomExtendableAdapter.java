package com.sinyolo.smartmerchandiser.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.sinyolo.smartmerchandiser.ErrorActivity;
import com.sinyolo.smartmerchandiser.FetchTask;
import com.sinyolo.smartmerchandiser.OrderActivity;
import com.sinyolo.smartmerchandiser.R;
import com.sinyolo.smartmerchandiser.SinyoloUtils;
import com.sinyolo.smartmerchandiser.model.CMSDto;
import com.sinyolo.smartmerchandiser.model.CMSRequestType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sinyolo.smartmerchandiser.SinyoloUtils.HEADER_DATA;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.SUPERMARKET;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.defaultMerchandiserActivity;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.isListEmpty;

/**
 * Created by leeroy on 9/3/2018.
 */

public class CustomExtendableAdapter<X, Y> extends BaseExpandableListAdapter {
    private Context _context;
    private List<X> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Y>> _listDataChild;

    public CustomExtendableAdapter(Context context, List<X> listDataHeader,
                                   HashMap<String, List<Y>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = getChild(groupPosition, childPosition).toString();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_list, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.childTextView);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return isListEmpty(this._listDataChild.get(this._listDataHeader.get(groupPosition))) ? 0 :
                this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return isListEmpty(this._listDataHeader) ? 0 : this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition).toString();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group_list, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.groupTextView);
        lblListHeader.setTypeface(null, Typeface.BOLD);

        if (headerTitle.contains("red")) {
            Button headerButton = (Button) convertView.findViewById(R.id.groupButton);
            headerButton.setVisibility(View.VISIBLE);

            SinyoloUtils.ORDER_STRING = headerTitle.replace("red","");

            headerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(_context, OrderActivity.class);
                    _context.startActivity(intent);
                }
            });
        }

        if (headerTitle.contains("green")) {
            lblListHeader.setBackgroundColor(Color.GREEN);
            headerTitle = headerTitle.replace("green", "");
        } else if (headerTitle.contains("amber")) {
            lblListHeader.setBackgroundColor(Color.rgb(254, 178, 4));//AMBER
            headerTitle = headerTitle.replace("amber", "");
        } else if (headerTitle.contains("red")) {
            lblListHeader.setBackgroundColor(Color.RED);
            headerTitle = headerTitle.replace("red", "");
        }

        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
