package com.sinyolo.smartmerchandiser;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sinyolo.smartmerchandiser.adapter.CustomAdapter;
import com.sinyolo.smartmerchandiser.adapter.CustomExtendableAdapter;
import com.sinyolo.smartmerchandiser.model.CMSDto;
import com.sinyolo.smartmerchandiser.model.CMSRequestType;
import com.sinyolo.smartmerchandiser.model.Merchandiser;
import com.sinyolo.smartmerchandiser.model.Stock;
import com.sinyolo.smartmerchandiser.model.StockActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.sinyolo.smartmerchandiser.SinyoloUtils.CHILD_DATA;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.ERROR;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.HEADER_DATA;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.IMSI_IN_USE;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.MERCHANDISER_NAME;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.SUPERMARKET;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.defaultMerchandiserActivity;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.defaultStock;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.defaultStockActivity;

/**
 * Created by leeroy on 6/4/2018.
 */

public class NavigationActivity extends AppCompatActivity {

    private TextView merchandiserText;
    private ListView mListView;
    private ImageView mImageView;
    private TextView mCustomTextView;
    private Context context;
    private CustomAdapter adapter;
    private CustomExtendableAdapter extendableAdapter;
    private ExpandableListView mExtendableListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private String supermarket;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_current_stock:
                    mCustomTextView.setText("STOCK");
                    mImageView.setImageResource(R.mipmap.stock);
                    getStockFromRemote();
                    if (listDataHeader == null) {
                        //load defaults in case of an error with remote
                        listDataHeader = new ArrayList<>();
                        listDataHeader = defaultStock(null);
                        listDataChild = new HashMap<>();
                    }
                    extendableAdapter = new CustomExtendableAdapter<String, String>(context, listDataHeader, listDataChild);
                    mExtendableListView.setAdapter(extendableAdapter);
                    return true;
                case R.id.navigation_stock_history:
                    mCustomTextView.setText("ORDER");
                    mImageView.setImageResource(R.mipmap.orders);
                    getStockHistoryFromRemote();
                    if (listDataHeader == null) {
                        //load defaults in case of an error with remote
                        listDataHeader = new ArrayList<>();
                        listDataHeader = defaultStock(new ArrayList<StockActivity>());
                        listDataChild = defaultStockActivity(listDataHeader);
                    }
                    extendableAdapter = new CustomExtendableAdapter<String, String>(context, listDataHeader, listDataChild);
                    mExtendableListView.setAdapter(extendableAdapter);
                    return true;
                case R.id.navigation_merchandiser_history:
                    mCustomTextView.setText("MERCHANDISER");
                    mImageView.setImageResource(R.drawable.ic_notifications_black_24dp);
                    getMerchandiserHistoryFromRemote();
                    if (listDataHeader == null) {
                        //load defaults in case of an error with remote
                        listDataHeader = new ArrayList<>();
                        listDataHeader.add("Sizo Sinyolo");
                        listDataChild = defaultMerchandiserActivity(listDataHeader);
                    }
                    extendableAdapter = new CustomExtendableAdapter<String, String>(context, listDataHeader, listDataChild);
                    mExtendableListView.setAdapter(extendableAdapter);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        merchandiserText = (TextView) findViewById(R.id.merchandiserTextView);
        mImageView = (ImageView) findViewById(R.id.customImageView);
        mCustomTextView = (TextView) findViewById(R.id.customTextView);
        mExtendableListView = (ExpandableListView) findViewById(R.id.list);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getStockFromRemote();
        extendableAdapter = new CustomExtendableAdapter<String, String>(this, listDataHeader, listDataChild);
        mExtendableListView.setAdapter(extendableAdapter);

        mCustomTextView.setText("STOCK");
        mImageView.setImageResource(R.mipmap.stock);

        merchandiserText.setText("Hello to you " + MERCHANDISER_NAME + " our trusted envoy to " + SUPERMARKET);

        context = this;

        supermarket = SUPERMARKET;
    }

    private void getStockHistoryFromRemote() {
        try {
            CMSDto cmsDto = defaultCmsDto();
            cmsDto.setType(CMSRequestType.STOCK_LOG_GET);
            cmsDto.getStock().setSupermarket(SUPERMARKET);
            FetchTask fetchTask = new FetchTask(cmsDto);
            while (fetchTask.isRunning()) {
                //do nothing
            }
            cmsDto = fetchTask.call();

            listDataChild = (HashMap<String, List<String>>) cmsDto.getAdditionalCrap().get(CHILD_DATA);
            listDataHeader = (List<String>) cmsDto.getAdditionalCrap().get(HEADER_DATA);

        } catch (Exception e) {
            e.printStackTrace();
            ERROR = "Exception: \n" + e.toString();
            Intent intent = new Intent(context, ErrorActivity.class);
            startActivity(intent);
        }
    }

    private void getStockFromRemote() {
        try {
            CMSDto cmsDto = defaultCmsDto();
            cmsDto.setType(CMSRequestType.STOCK_GET);
            cmsDto.getStock().setSupermarket(SUPERMARKET);
            FetchTask fetchTask = new FetchTask(cmsDto);
            while (fetchTask.isRunning()) {
                //do nothing
            }
            cmsDto = fetchTask.call();

            listDataHeader = (List<String>) cmsDto.getAdditionalCrap().get(HEADER_DATA);

        } catch (Exception e) {
            e.printStackTrace();
            ERROR = "Exception: \n" + e.toString();
            Intent intent = new Intent(context, ErrorActivity.class);
            startActivity(intent);
        }
    }

    private void getMerchandiserHistoryFromRemote() {
        try {
            CMSDto cmsDto = defaultCmsDto();
            cmsDto.setType(CMSRequestType.MERCHANDISER_LOG_GET);
            cmsDto.getMerchandiser().setImsi(IMSI_IN_USE);

            FetchTask fetchTask = new FetchTask(cmsDto);
            while (fetchTask.isRunning()) {
                //do nothing
            }
            cmsDto = fetchTask.call();

            listDataChild = (HashMap<String, List<String>>) cmsDto.getAdditionalCrap().get(CHILD_DATA);
            listDataHeader = (List<String>) cmsDto.getAdditionalCrap().get(HEADER_DATA);

        } catch (Exception e) {
            e.printStackTrace();
            ERROR = "Exception: \n" + e.toString();
            Intent intent = new Intent(context, ErrorActivity.class);
            startActivity(intent);
        }
    }

    private CMSDto defaultCmsDto(){
        CMSDto cmsDto = new CMSDto();
        cmsDto.setMerchandiser(new Merchandiser());
        cmsDto.getMerchandiser().setName(SinyoloUtils.MERCHANDISER_NAME);
        cmsDto.getMerchandiser().setImsi(IMSI_IN_USE);

        return cmsDto;
    }
}
