package com.sinyolo.smartmerchandiser;

import android.os.AsyncTask;

import com.sinyolo.smartmerchandiser.model.CMSDto;

import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.concurrent.Callable;

import static com.sinyolo.smartmerchandiser.SinyoloUtils.*;

/**
 * Created by leeroy on 9/3/2018.
 */

public class FetchTask implements Callable<CMSDto> {
    private CMSDto finalCmsDto;
    private boolean running = true;
    private Exception exception;

    public FetchTask(CMSDto cmsDto) {
        finalCmsDto = cmsDto;
        new CMSAsyncTask(cmsDto).execute(new String[]{});
    }

    @Override
    public CMSDto call() throws Exception {
        if (isRunning() && exception != null) {
            throw new Exception(exception.toString());
        }
        return finalCmsDto;
    }

    public CMSDto getFinalCmsDto() {
        return finalCmsDto;
    }

    public void setFinalCmsDto(CMSDto cmsDto) {
        this.finalCmsDto = cmsDto;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    private class CMSAsyncTask extends AsyncTask<String, Void, CMSDto> {

        private CMSDto cmsDto;

        CMSAsyncTask(CMSDto cmsDto) {
            this.cmsDto = cmsDto;
        }

        @Override
        protected CMSDto doInBackground(String... strings) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                if (cmsDto != null) {
                    cmsDto = restTemplate.postForObject(REMOTE_URL, cmsDto, CMSDto.class);
                }
                running = false;
                finalCmsDto = cmsDto;
            } catch (Exception e) {
                exception = e;
            }
            return cmsDto;
        }

        protected void onPostExecute(CMSDto cmsDto) {
            // TODO: check this.exception
            // TODO: do something with the feed
            finalCmsDto = cmsDto;
            running = false;
        }
    }

}
