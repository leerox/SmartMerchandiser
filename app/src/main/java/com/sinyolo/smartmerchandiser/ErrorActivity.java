package com.sinyolo.smartmerchandiser;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import static com.sinyolo.smartmerchandiser.SinyoloUtils.ERROR;

/**
 * Created by leeroy on 18/4/2018.
 */

public class ErrorActivity extends AppCompatActivity {

    private TextView mErrorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Make sure this is before calling super.onCreate
        super.onCreate(savedInstanceState);
        setContentView(R.layout.error_layout);

        mErrorTextView = (TextView) findViewById(R.id.errorTextView);

        mErrorTextView.setText(ERROR);
    }
}
