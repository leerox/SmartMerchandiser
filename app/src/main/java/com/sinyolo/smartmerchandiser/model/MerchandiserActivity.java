package com.sinyolo.smartmerchandiser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by leeroy on 9/3/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MerchandiserActivity extends CMSModelBase {
    private String merchandiserId;
    private String activity;

    public String getMerchandiserId() {
        return merchandiserId;
    }

    public void setMerchandiserId(String merchandiserId) {
        this.merchandiserId = merchandiserId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return activity + "\n" + updated;
    }
}
