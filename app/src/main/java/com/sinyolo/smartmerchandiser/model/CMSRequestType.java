package com.sinyolo.smartmerchandiser.model;

/**
 * Created by leeroy on 14/4/2018.
 */
public class CMSRequestType {

    public static final String MERCHANDISER_LOGIN = "MERCHANDISER_LOGIN";
    public static final String MERCHANDISER_LOG_GET = "MERCHANDISER_LOG_GET";
    public static final String MERCHANDISER_LOG_UPDATE = "MERCHANDISER_LOG_UPDATE";
    public static final String STOCK_LOG_GET = "STOCK_LOG_GET";
    public static final String STOCK_LOG_UPDATE = "STOCK_LOG_UPDATE";
    public static final String STOCK_GET = "STOCK_GET";
    public static final String STOCK_UPDATE = "STOCK_UPDATE";
    public static final String ORDER_CREATE            = "ORDER_CREATE";
}
