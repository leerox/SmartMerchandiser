package com.sinyolo.smartmerchandiser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by leeroy on 14/4/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CMSDto {

    private Map<String, Object> additionalCrap;
    private Merchandiser merchandiser;
    private Order order;
    private Stock stock;
    private String type;

    public Map<String, Object> getAdditionalCrap() {
        if (additionalCrap == null) {
            additionalCrap = new HashMap<>();
        }
        return additionalCrap;
    }

    public void setAdditionalCrap(Map<String, Object> additionalCrap) {

        this.additionalCrap = additionalCrap;
    }

    public Merchandiser getMerchandiser() {

        if (merchandiser == null) {
            merchandiser = new Merchandiser();
        }
        return merchandiser;
    }

    public void setMerchandiser(Merchandiser merchandiser) {

        this.merchandiser = merchandiser;
    }

    public Order getOrder() {

        if (order == null) {
            order = new Order();
        }
        return order;
    }

    public void setOrder(Order order) {

        this.order = order;
    }

    public Stock getStock() {

        if (stock == null) {
            stock = new Stock();
        }
        return stock;
    }

    public void setStock(Stock stock) {

        this.stock = stock;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {

        this.type = type;
    }
}
