package com.sinyolo.smartmerchandiser.model;

import java.util.Date;

/**
 * Created by leeroy on 18/4/2018.
 */
public class CMSModelBase {

    protected Date created;
    protected Date deleted;
    protected String id;
    protected Date updated;

    public CMSModelBase() {
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}

