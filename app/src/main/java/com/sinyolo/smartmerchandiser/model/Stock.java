package com.sinyolo.smartmerchandiser.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by leeroy on 9/3/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stock extends CMSModelBase {
    private String name;
    private Long quantity;
    private BigDecimal value;
    private long alarmQuantity;
    private long optimumQuanity;
    private String supermarket;
    @JsonIgnore
    private List<StockActivity> stockHistory;

    public long getAlarmQuantity() {
        return alarmQuantity;
    }

    public void setAlarmQuantity(long alarmQuantity) {
        this.alarmQuantity = alarmQuantity;
    }

    public long getOptimumQuanity() {
        return optimumQuanity;
    }

    public void setOptimumQuanity(long optimumQuanity) {
        this.optimumQuanity = optimumQuanity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public List<StockActivity> getStockHistory() {
        return stockHistory;
    }

    public void setStockHistory(List<StockActivity> stockHistory) {
        this.stockHistory = stockHistory;
    }

    public String getSupermarket() {
        return supermarket;
    }

    public void setSupermarket(String supermarket) {
        this.supermarket = supermarket;
    }

    @Override
    public String toString() {
        String color;
        if (quantity <= alarmQuantity) {
            color = "red";
        } else if (quantity <= optimumQuanity) {
            color = "amber";
        } else {
            color = "green";
        }

        if (stockHistory == null) {
            return color + name + "\n" + "Quantity:     " + quantity + "\n" + "Value:       $" + value;
        } else {
            return color + name;
        }
    }
}
