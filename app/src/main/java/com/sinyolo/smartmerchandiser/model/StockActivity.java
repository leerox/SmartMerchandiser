package com.sinyolo.smartmerchandiser.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by leeroy on 9/3/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockActivity extends CMSModelBase {
    private String stockId;
    @JsonIgnore
    private String stockName;
    private String activity;
    private String activityBy;

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivityBy() {
        return activityBy;
    }

    public void setActivityBy(String activityBy) {
        this.activityBy = activityBy;
    }

    @Override
    public String toString() {
        return "Activity:   "+activity + "\n" + "By:    " + activityBy + "\nDate:   " + updated;
    }
}
