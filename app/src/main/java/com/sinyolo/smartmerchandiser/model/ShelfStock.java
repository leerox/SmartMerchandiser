package com.sinyolo.smartmerchandiser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

/**
 * Created by leeroy on 13/3/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShelfStock extends CMSModelBase {

	private String     stockId;
	private Long       quantity;
	private BigDecimal value;
	private Long       alarmQuantity;
	private Long       optimumQuanity;

	public Long getAlarmQuantity() {

		return alarmQuantity;
	}

	public void setAlarmQuantity(Long alarmQuantity) {

		this.alarmQuantity = alarmQuantity;
	}

	public Long getOptimumQuanity() {

		return optimumQuanity;
	}

	public void setOptimumQuanity(Long optimumQuanity) {

		this.optimumQuanity = optimumQuanity;
	}

	public String getStockId() {

		return stockId;
	}

	public void setStockId(String stockId) {

		this.stockId = stockId;
	}

	public Long getQuantity() {

		return quantity;
	}

	public void setQuantity(Long quantity) {

		this.quantity = quantity;
	}

	public BigDecimal getValue() {

		return value;
	}

	public void setValue(BigDecimal value) {

		this.value = value;
	}
}
