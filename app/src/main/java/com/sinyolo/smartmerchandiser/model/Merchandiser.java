package com.sinyolo.smartmerchandiser.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by leeroy on 9/3/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Merchandiser extends CMSModelBase {
    private String name;
    private String superMarket;
    private String imsi;
    private List<MerchandiserActivity> merchandiserHistory;

    public List<MerchandiserActivity> getMerchandiserHistory() {
        return merchandiserHistory;
    }

    public void setMerchandiserHistory(List<MerchandiserActivity> merchandiserHistory) {
        this.merchandiserHistory = merchandiserHistory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuperMarket() {
        return superMarket;
    }

    public void setSuperMarket(String superMarket) {
        this.superMarket = superMarket;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }
}
