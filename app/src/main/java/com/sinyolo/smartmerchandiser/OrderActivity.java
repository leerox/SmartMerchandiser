package com.sinyolo.smartmerchandiser;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sinyolo.smartmerchandiser.model.CMSDto;
import com.sinyolo.smartmerchandiser.model.CMSRequestType;
import com.sinyolo.smartmerchandiser.model.Merchandiser;
import com.sinyolo.smartmerchandiser.model.Order;
import com.sinyolo.smartmerchandiser.model.Stock;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

import static com.sinyolo.smartmerchandiser.SinyoloUtils.CHILD_DATA;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.ERROR;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.HEADER_DATA;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.IMSI_IN_USE;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.SUPERMARKET;

/**
 * Created by leeroy on 3/5/2018.
 */

public class OrderActivity extends AppCompatActivity {

    private TextView stockText;
    private TextView promptText;
    private EditText quantityText;
    private Button placeOrderButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        stockText = (TextView) findViewById(R.id.orderTextViewStock);
        promptText = (TextView) findViewById(R.id.orderTextViewQuantityPrompt);
        quantityText = (EditText) findViewById(R.id.editTextQuantity);
        placeOrderButton = (Button) findViewById(R.id.orderButton);

        stockText.setText(SinyoloUtils.ORDER_STRING);
        promptText.setText("Enter the quantity you would like to order below: ");
    }

    public void onClickOrder(View v){
        try {
            CMSDto cmsDto = new CMSDto();
            cmsDto.setType(CMSRequestType.ORDER_CREATE);
            Order order = new Order();
            order.setStockId(SinyoloUtils.ORDER_STRING);
            order.setQuantity(Long.valueOf(quantityText.getText().toString()));
            cmsDto.setOrder(order);
            cmsDto.setStock(new Stock());
            cmsDto.getStock().setSupermarket(SinyoloUtils.SUPERMARKET);
            cmsDto.setMerchandiser(new Merchandiser());
            cmsDto.getMerchandiser().setName(SinyoloUtils.MERCHANDISER_NAME);
            cmsDto.getMerchandiser().setImsi(IMSI_IN_USE);
            FetchTask fetchTask = new FetchTask(cmsDto);
            while (fetchTask.isRunning()) {
                //do nothing
            }
            fetchTask.call();

            Intent intent = new Intent(this, NavigationActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            ERROR = "Exception: \n" + e.toString();
            Intent intent = new Intent(this, ErrorActivity.class);
            startActivity(intent);
        }
    }
}
