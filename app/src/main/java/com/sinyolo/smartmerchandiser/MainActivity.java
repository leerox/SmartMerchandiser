package com.sinyolo.smartmerchandiser;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.sinyolo.smartmerchandiser.model.CMSDto;
import com.sinyolo.smartmerchandiser.model.CMSRequestType;
import com.sinyolo.smartmerchandiser.utils.ImsiProperty;

import static com.sinyolo.smartmerchandiser.SinyoloUtils.ERROR;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.IMSI_1;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.IMSI_2;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.IMSI_IN_USE;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.MERCHANDISER_NAME;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.SUPERMARKET;
import static com.sinyolo.smartmerchandiser.SinyoloUtils.getImsiProperty;

public class MainActivity extends AppCompatActivity {

    private ImageView mSplashImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Make sure this is before calling super.onCreate
        super.onCreate(savedInstanceState);

        mSplashImageView = (ImageView) findViewById(R.id.splashImageView);

        try {
            Thread.sleep(2000);
            routeToAppropriatePage();
            finish();
        } catch (InterruptedException e) {
            routeToAppropriatePage();
            finish();
        }
    }

    private void routeToAppropriatePage() {
        try {
            CMSDto cmsDto = new CMSDto();
            cmsDto.setType(CMSRequestType.MERCHANDISER_LOGIN);
            ImsiProperty imsiProperty = getImsiProperty(this.getBaseContext());
            if (imsiProperty.getImsis() == null) {
                if (imsiProperty.getImsis().get(IMSI_1) != null) {
                    cmsDto.getMerchandiser().setImsi(imsiProperty.getImsis().get(IMSI_1));
                } else {
                    cmsDto.getMerchandiser().setImsi(imsiProperty.getImsis().get(IMSI_2));
                }
            } else {
                cmsDto.getMerchandiser().setImsi(imsiProperty.getImsi());
            }
            String android_id = Settings.Secure.getString(this.getBaseContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            cmsDto.getMerchandiser().setImsi(android_id);
            IMSI_IN_USE = cmsDto.getMerchandiser().getImsi();

            cmsDto.getMerchandiser().setName("Dummy");

            FetchTask fetchTask = new FetchTask(cmsDto);
            while (fetchTask.isRunning()){
                //do nothing
            }
            cmsDto = fetchTask.call();

            if (cmsDto.getMerchandiser() == null) {

                //TODO create 404 activity
                Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
                startActivity(intent);
            } else {

                SUPERMARKET = cmsDto.getMerchandiser().getSuperMarket();
                MERCHANDISER_NAME = cmsDto.getMerchandiser().getName();
                Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
                startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ERROR = "Exception: \n" + e.toString();
            Intent intent = new Intent(MainActivity.this, ErrorActivity.class);
            startActivity(intent);
        }
    }
}
