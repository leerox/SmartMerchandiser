package com.sinyolo.smartmerchandiser.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by leeroy on 18/4/2018.
 */

public class ImsiProperty {
    public String imsi;
    public Map<String, String> imsis;

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Map<String, String> getImsis() {
        if (imsis == null) {
            imsis = new HashMap<>();
        }
        return imsis;
    }

    public void setImsis(Map<String, String> imsis) {
        this.imsis = imsis;
    }
}
