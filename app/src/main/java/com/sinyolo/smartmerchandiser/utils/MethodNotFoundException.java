package com.sinyolo.smartmerchandiser.utils;

/**
 * Created by leeroy on 18/4/2018.
 */

public class MethodNotFoundException extends Exception {
    public MethodNotFoundException(String message){
        super(message);
    }
}
